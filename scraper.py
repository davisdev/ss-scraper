#! /usr/bin/python3
import requests
import time
import os, sys
from argparse import ArgumentParser
from bs4 import BeautifulSoup as bs
from termcolor import colored

parser = ArgumentParser(
    prog='./scraper',
    description='Pull fresh vacancies from SS.com.',
    epilog='Tip: In order to run the script like intended, open terminal and type {}.'.format(
        colored('sudo chmod +x scraper.py', attrs=['bold']))
)

parser.add_argument('--interval', help='Time (in sec) after which to pull data again', default='300', type=int)
parser.add_argument('--type', help='Speciality (electrician, programmer etc.) you are looking for')
parser.add_argument('--limit', help='Show only specified amount of latest vacancies', type=int)
parser.add_argument('--sound', help='Play sound when new job ad appears', default='notification')

args = parser.parse_args()

specialities = ['actor', 'administrator', 'advertising agent', 'adviser', 'agent', 'agronomist', 'ameliator', 'analyst', 'appraiser', 'architect', 'armature fitter', 'artist', 'assembler','assistant', 'attendant', 'autocrane operator', 'autoelectrician', 'autohouse painter', 'automechanic', 'autoinman', 'autowasher', 'aviation specialist',
'baker', 'barman', 'bartender', 'bathouse attendant', 'bookkeeper', 'breadborder', 'broker', 'builder', 'bulldozer driver', 'butcher',
'car mechanician', 'carpenter', 'cashier', 'chief', 'cleaner', 'clerk', 'climber', 'collector', 'commodity superintendent', 'constructor', 'cow milker', 'controller', 'cook', 'copyrighter', 'cosmetician', 'courier', 'crane operator', 'croupier',
'dancer', 'declarer', 'dancer', 'designer', 'director', 'dispatcher', 'docker', 'doctor', 'doctor assistant', 'dredger', 'driver',
'e forwarding agent', 'economist', 'educator', 'electric welder', 'electrician', 'electromechanics', 'engineer', 'estimater', 'expert',
'financial analyst', 'fish processor', 'florist', 'foreman', 'furniturer',
'gardener', 'glazier', 'guide',
'hairdresser', 'handyman', 'house painter', 'housemaid',
'illustrator', 'instructor', 'instructor', 'insurance agent', 'interpreter',
'jeweller', 'joiner', 'journalist',
'knitter',
'lawyer', 'lecturer', 'loader', 'logist', 'linux administrator',
'manager', 'manicurist', 'mason', 'masseur', 'mechanic', 'mechanics', 'med nurse', 'milling-machine operator', 'model', 'musician',
'network administrator', 'nurse',
'operator', 'other',
'packer', 'panman', 'parlourmaid', 'pave makers', 'personnel manager', 'pharmacist', 'philologist', 'photographer', 'plasterer', 'plater', 'porter, doorman', 'printing house worker', 'programmer', 'provider', 'psychologist', 'pupil',
'radio engineer', 'real estate agent', 'redactor', 'reviewer', 'roofer',
'sailor', 'sanitary technician', 'sawer', 'seaman', 'seamstress', 'secretary', 'security guard', 'seller', 'service center master', 'serviceman', 'sorter', 'steward', 'stomatologist', 'storekeeper', 'stovemaker', 'student', 'surveyor',
'tailor', 'teacher', 'technical support', 'technologist', 'tinman', 'tour operator', 'tractor operator', 'trainer', 'turner', 'typesetter',
'utensils washer', 'varnisher', 'veterinary', 'visagist', 'vocalist',
'waiter', 'washerman', 'welder', 'windows master', 'woodcutter', 'worker', 'web designer',
'yard keeper']

if args.type not in specialities:
    raise ValueError('There is no such speciality "{}" available'.format(args.type))

# Be kind to the website. We don't want to even lightly DOS it.
if args.interval < 10:
    print('We do not want to DOS the site. Lowering to 5 min.')
    time.sleep(3)
    args.interval = 60*5
    os.system('clear')

config = {
    'vacancy_base_url': 'https://ss.com/lv/work/are-required/{}/'.format(args.type),
    'type': 'programmer' if args.type is None else args.type,
    'interval': 60*5 if args.interval is None else args.interval,  # 5 minutes
    'limit': 5 if args.limit is None else args.limit,
    'retry_after': 10
}

'''
    Getting the HTML response which we then convert into
    'soup' upon which we can interact with tags.
'''
def scrape():
    html = requests.get(config.get('vacancy_base_url'))
    soup = bs(html.text, 'html.parser') # TODO Narrow down to only the needed table

    filters = soup.select_one('table#page_main table')
    vacancy_table = filters.find_next_sibling('table')

    return vacancy_table.find_all('a', { 'class': 'am' })

def output(scraped_data):
    print('Speciality: {type}, showing latest {limit} results.'.format(
        type=colored(config.get('type'), attrs=['bold']), limit=config.get('limit')))
    print('\n')

    for vacancy in scraped_data[:config.get('limit')]:
        print(colored(clear_whitespace(vacancy.text) + '...', color='blue', on_color='on_white', attrs=['bold']))
        print(colored('[https://ss.com{}]'.format(vacancy.get("href")), color='white', attrs=['bold']))
        print('\n\n<' + '*=' * 35 + '*>\n\n')

def check_connection():
    site_for_check = 'https://google.com'
    timeout = 5 # how long to wait before calling it

    try:
        requests.get(site_for_check, timeout=timeout)
        
        return True
    except requests.ConnectionError:
        print('Make sure you are connected to the internet in order to start the program!')
        retry_after = config.get('retry_after')

        if retry_after is not None:
            print('Retrying in {} seconds.'.format(config.get('retry_after')))
            time.sleep(config.get('retry_after'))
            os.system('clear')
            print('Trying to reconnect...')
            time.sleep(1)
            main()

        sys.exit(-1)

def clear_whitespace(text):
    return " ".join(str(text).split())

def main():
    try:
        while True:
            os.system('clear')
            check_connection()
            scraped_data = scrape()
            if len(scraped_data) < 1:
                print('No vacancies available for {}'.format(
                    colored(config.get('type'), attrs=['bold'])))
            else:
                output(scraped_data)

            print('Update: in {} seconds'.format(config.get('interval')))
            print('Press {} to stop the program.'.format(
                colored('Ctrl+c', attrs=['bold'])))

            time.sleep(config.get('interval'))
            os.system('clear')
            print('Pulling data...')
            time.sleep(1)
    except KeyboardInterrupt:
        print('Thanks for using scraper!')
        print('Make sure to star the scraper if you find this helpful: ' +
              colored('https://gitlab.com/davisdev/ss-scraper', on_color='on_cyan', attrs=['bold']))
        sys.exit(0)


if __name__ == "__main__":
    main()
    
