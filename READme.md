# How it looks
![How it works](https://i.imgur.com/DW9Nc9F.gif)

# Purpose
You feel lazy and tired after a long day of doing... not much? Same.
So why not make scavenging for new vacancies more fun (code word for `automated`)?

Use this scraper in console for pulling the freshest underpaid jobs from SS.com (will probably extend that to use LikeIT as a different provider / source).

## Installation
Just clone this repository, _cd_ into it, give that sucker (scraper.py) a permission to be executed (`sudo chmod +x scraper.py`) and run `./scraper -h` to bless
your eyes with the nicely formatted help menu and you should be good to go.

## Todo
- [] For speciality type there still should be functionality to compensate alternative forms / typos, since for now types are strictly checked to the inner list of specialities and doesn't conform to case sensitive checking, as well as some specialities might be written in different way (like 'wood cutter' vs 'woodcutter')
- [] Tests
- [] Optionally, a way to send e-mail on new vacancies
- [] Probably better design
- [] Convert to code to be OO?
- [] Option to provide data providing site
- [] Add requirements.txt which I forgot
- [] Dockerize